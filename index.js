//use path module
const path = require('path');
//use express module
const express = require('express');
//use hbs view engine
const hbs = require('hbs');
//use bodyParser middleware
const bodyParser = require('body-parser');
//cors
const cors = require('cors')
//firebase
const firebaseapp = require('firebase/app');
const firebase = require('firebase');
const firestore = require('firebase/firestore');

const app = express();
app.use(cors())
app.use(bodyParser.json())
// Firebase config
const firebaseConfig = {
    apiKey: "AIzaSyAkzbogQ5WaWVDHCqvfg3oMcjz0aw5bLME",
    authDomain: "latihan-be04-16.firebaseapp.com",
    databaseURL: "https://latihan-be04-16.firebaseio.com",
    projectId: "latihan-be04-16",
    storageBucket: "latihan-be04-16.appspot.com",
    messagingSenderId: "145707821819",
    appId: "1:145707821819:web:c0cbb191982c6f6fedde5e",
    measurementId: "G-082D1K3ERJ"
};
// initialize firebase
firebase.initializeApp(firebaseConfig);
// db and firestore
var db = firebase.database()
var fs = firebase.firestore()

//set views file
app.set('views',path.join(__dirname,'views'));
//set view engine
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//set folder public as static folder for static file
app.use('/assets',express.static(__dirname + '/public'));

//route untuk homepage
app.get('/',(req, res) => {
    res.send('<pre>halaman antrian ada di url /antrian/{service}</pre>')    
});

let csRef = fs.collection('antrian').doc('customer_service');
let telRef = fs.collection('antrian').doc('teller');
fs.settings({
    timestampsInSnapshots:true
})
/*
var datas = []
csRef.onSnapshot(docSnapshot => {
    //console.log(`Received doc snapshot: ${docSnapshot.data()}`);
    docSnapshot.docChanges().forEach(dat =>{
        datas.push(dat.doc.data())
    })
  }, err => {
    console.log(`Encountered error: ${err}`);
  });
  */
app.get('/antrian/cs',(req, res) => {
    csRef.onSnapshot(docSnapshot => {
        //console.log(`Received doc snapshot: ${docSnapshot.data()}`);
        var datas = docSnapshot.data()
        res.send(datas)
      }, err => {
        console.log(`Encountered error: ${err}`);
      });
})

app.get('/antrian/teller',(req, res) => {
    fs.settings({
        timestampsInSnapshots:true,
    })
    telRef.onSnapshot(docSnapshot => {
            //console.log(`Received doc snapshot: ${docSnapshot.data()}`);
        var dat = docSnapshot.data()
        res.send(dat)
    }, err => {
        console.log(`Encountered error: ${err}`);
    });
})


//server listening
app.listen(3000, () => {
    console.log('Server is running at port 3000');
});